package io.lhyz.android.cameratemplate;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

/**
 * 只实现了正常的预览，对焦和保存功能，保存图形也没有移到另外的线程，所以按下拍照之后会有点卡顿
 */
public class MainActivity extends AppCompatActivity implements
        SurfaceHolder.Callback, View.OnClickListener,
        Camera.AutoFocusCallback, Camera.PictureCallback {

    private static final int REQUEST_SAVE = 0x1;

    HandlerThread mHandlerThread;
    Handler mHandler;

    SurfaceView mSurfaceView;
    SurfaceHolder mSurfaceHolder;
    Camera mCamera;

    final Activity mActivity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        mHandlerThread = new HandlerThread("MainActivity");
        mHandlerThread.start();
        mHandler = new Handler(mHandlerThread.getLooper()) {
            @Override
            public void handleMessage(Message msg) {
                if (REQUEST_SAVE == msg.what) {
                    byte[] data = (byte[]) msg.obj;
                    Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                    File path;
                    String route;
                    if ((path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)).exists()) {
                        route = path.getAbsolutePath() + "/" + UUID.randomUUID() + ".png";
                        try (OutputStream output = new FileOutputStream(route)) {
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };

        mSurfaceView = (SurfaceView) findViewById(R.id.surface);
        assert mSurfaceView != null;
        mSurfaceView.setOnClickListener(this);
        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceHolder.setKeepScreenOn(true);
        mSurfaceHolder.addCallback(this);

        View view = findViewById(R.id.take);
        assert view != null;
        view.setOnClickListener(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (mCamera == null) {
            mCamera = Camera.open();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Camera.Parameters parameters = mCamera.getParameters();
        parameters.setPictureFormat(ImageFormat.JPEG);
        if (mActivity.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
            parameters.set("orientation", "portrait");
            mCamera.setDisplayOrientation(90);
            parameters.setRotation(90);
        } else {
            parameters.set("orientation", "landscape");
            mCamera.setDisplayOrientation(90);
            parameters.setRotation(0);
        }

        mCamera.setParameters(parameters);
        try {
            mCamera.setPreviewDisplay(mSurfaceHolder);
        } catch (IOException e) {
            mCamera.release();
            mCamera = null;
        }

        if (mCamera != null) {
            mCamera.startPreview();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mCamera.stopPreview();
        mCamera.release();
        mCamera = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.surface:
                mCamera.autoFocus(this);
                break;
            case R.id.take:
                mCamera.takePicture(null, null, null, this);
        }
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        Message msg = mHandler.obtainMessage(REQUEST_SAVE, data);
        msg.sendToTarget();
        mCamera.startPreview();
    }

    @Override
    public void onAutoFocus(boolean success, Camera camera) {

    }

    @Override
    protected void onPause() {
        super.onPause();
        mHandlerThread.quit();
        mHandlerThread.interrupt();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}